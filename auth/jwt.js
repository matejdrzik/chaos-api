const TokenCache = require('./token_cache');
const tokenCache = new TokenCache();

const INVALID_TOKEN = 'INVALID_TOKEN';

const respondWithInvalidToken = (res) => {
  res.statusCode = 403;
  res.setHeader('content-type', 'application/json');
  res.write(JSON.stringify({ message: 'Invalid Token', code: 403 }));
  res.end(); 
};

module.exports = (db) => {
  const Users = db.get('users');
  const Tokens = db.get('tokens');
  // const Users = r.table('users');
  // const Tokens = r.table('tokens');

  return (req, res, next) => {
    if (req.isWhitelisted) {
      next();
    } else {
      if (req.headers.hasOwnProperty('authorization')) {
        // extract jwt from token
        const token = req.headers.authorization.replace('Bearer ', '');
        // check for cache presence
        if (tokenCache.has(token)) {
          if (tokenCache.get(token) == INVALID_TOKEN) {
            respondWithInvalidToken(res);
          } else {
            req.user = tokenCache.get(token);
            next();
          }
        } else {
          // fetch from rethink if cache is not present
          const user = Tokens
            .find({ id: token })
            .map((token) => {
              return Users.find({ id: token.user_id }).value();
            })
            .value();

          console.log('token results', user);
          
          if (user) {
            req.user = user;
            tokenCache.set(token, user); 
            next();
          } else {
            tokenCache.set(token, INVALID_TOKEN);
            respondWithInvalidToken(res);
          }

        }
      } else {
        // in case authorization header is not present at all, decline all access
        res.statusCode = 401;
        res.setHeader('content-type', 'application/json');
        res.write(JSON.stringify({message: 'Unauthorized', code: 401 }));
        res.end();
      }
    }
  };
};
