class TokenCache {
    constructor() {
        this.tokens = {};
    }

    has(token) {
        return this.tokens.hasOwnProperty(token);
    }

    get(token) {
        return this.tokens[token];
    }

    set(token, value) {
        this.tokens[token] = value;
    }    
}

module.exports = TokenCache;