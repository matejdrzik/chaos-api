const express = require('express');
const _ = require('lodash');

module.exports = (whitelist) => {
  return (req, res, next) => {
    if (_.includes(whitelist, req.path)) {
      req.isWhitelisted = true;
    }
    next();
  };
}
