const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const uuid = require('node-uuid');
const moment = require('moment');
const _ = require('lodash');
const Settings = require('./settings');
const low = require('lowdb');
const db = low('db.json', { storage: require('lowdb/lib/file-async') });
db._.mixin(require('underscore-db'));

const jwtAuth = require('./auth/jwt')(db);
const whitelister = require('./auth/whitelister');


// Create default port
const PORT = Settings.port;

// Create a new app
const app = express();
const apiRoutes = express.Router();


// LowDB: Check if tables exists
const tables = ['users', 'tokens', 'beacons'];
let dbInit = false;
tables.forEach((table) => {
  if (!db.has(table).value()) {
    dbInit = true;
    db.set(table, []).value();
    console.log('created table', table);
  }
});

if (dbInit) {
  const user = {
    id: uuid.v4(),
    name: 'Demo User',
    email: 'john@doe.com',
    password: 'test1234',
    createdAt: new Date(),
    updatedAt: new Date()
  };
  const token = {
    id: uuid.v4(),
    user_id: user.id,
    createdAt: new Date(),
    updatedAt: new Date()
  };
  const beacon = {
    id: uuid.v4(),
    name: 'Test Beacon #1',
    uuid: "f7826da6-4fa2-4e98-8024-bc5b71e0893e",
    major: 1234,
    minor: 1200,
    createdAt: new Date(),
    updatedAt: new Date()
  }
  db.get('users').insert(user).value();
  db.get('tokens').insert(token).value();
  db.get('beacons').insert(beacon).value();
  console.log(`Demo User created with email: ${user.email}, password: ${user.password}`);
}

// Basic Request Logger
app.use((req, res, next) => {
  console.log(new Date(), req.method, req.path);
  next();
});

app.use(cors());
// Configure app
app.use(bodyParser.json())
   .use(bodyParser.urlencoded({ extended: false }));

// Whitelist requests
app.use(whitelister(Settings.whitelist));

// Authorize all requests with JWT, skip whitelisted
app.use(jwtAuth);

apiRoutes.post('/auth/login', (req, res) => {
  const params = req.body;

  const user = db.get('users').find({ email: params.email, password: params.password }).value();
  console.log('found user:', user);
  if (!user) {
    res.status(401).send("Invalid Credentials");
  } else {
    const token = db.get('tokens').find({ user_id: user.id }).value();
    if (!token) {
      res.status(401).send("Missing Token");
    } else {
      console.log('response', {
        token: token.id,
        user: user
      });
      res.send({
        token: token.id,
        user: user
      }); 
    }
  }

});

require('./controllers/beacons')(db, apiRoutes);
require('./controllers/users')(db, apiRoutes);
app.use('/v1', apiRoutes);

// Attach your endpoints/controllers here
app.listen(PORT, () => {
  console.log(`The API is running on port ${PORT}`);
});
