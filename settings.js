module.exports = {
  database: 'chaos',
  port: 3000,
  whitelist: [ 
    '/v1/auth/login'
  ]
};