const uuid = require('uuid');

module.exports = (db, router) => {

  const Beacons = db.get('beacons');

  router.get('/beacons', (req, res) => {
    const beacons = Beacons.sortBy('name').value();
    res.send(beacons);    
  });

  router.get('/beacons/:id', (req, res) => {
    const beacon = Beacons.find({ id: req.params.id });
    res.send(beacon);    
  });

  router.post('/beacons', (req, res) => {
    const data = req.body;
    data.createdAt = new Date();
    data.updatedAt = data.createdAt;
    data.id = uuid.v4();

    Beacons.insert(data).value();
    res.send(data);    
  });

  router.put('/beacons/:id', (req, res) => {
    const data = req.body;
    data.updatedAt = new Date();
    console.log('should save data:', data);
    const beacon = Beacons.find({ id: req.params.id }).assign(data).value();
    console.log('beacon:', beacon);
    res.send(beacon);
  });

  router.delete('/beacons/:id', (req, res) => {    
    Beacons.remove({ id: req.params.id }).value();
    res.send({ success: true, id: req.params.id });      
  });
  
};
