module.exports = (db, router) => {

  // const Users = r.table('users');
  const Users = db.get('users');

  router.get('/users', (req, res) => {
    const users = Users.sortBy('name').value();
    res.send(users);    
  });

  router.get('/users/:id', (req, res) => {
    const user = Users.find({ id: req.params.id });
    if (user) {
      res.send(user);
    } else {
      res.status(404).send("NOT_FOUND");
    }    
  });

  router.post('/users', (req, res) => {
    const data = req.body;
    data.createdAt = new Date();
    data.updatedAt = data.createdAt;
    data.id = uuid.v4();

    Users.insert(data).value();
    Tokens.insert({ id: uuid.v4(), user_id: user.id }).value();

    res.send(user);    
  });

  router.put('/users/:id', (req, res) => {
    const data = req.body;
    data.updatedAt = new Date();
    Users.find({ id: req.params.id }).assign(data).value();
    res.send(data);
  });

  router.delete('/users/:id', (req, res) => {    
    Users.remove({ id: req.params.id }).value();
    res.send({ success: true, id: req.params.id });
  });
  
};