# Chaos API

## RethinkDB
You will need to create rethink database & tables by yourself. Open your RethinkDB WebUI at http://localhost:8080,
create a database, then "tokens", "users" and "beacon" tables. In "tokens" table add secondary index "user_id", in "users" table add
secondary index "email". After that click "Data Explorer" view and enter:

```
r.db('YOUR_DATABASE_NAME_HERE')
 .table('users')
 .insert({
   id: '1',
   name: 'John Doe',
   email: 'john@doe.com',
   password: 'testpassword'
 })
```

and

```
r.db('chaos')
  .table('tokens')
  .insert({
    id: 'TEST_TOKEN_FOR_USER_1',
    user_id: '1'
  })
```
## Installation

```
  npm install
```

## Usage

```
  node ./index.js
```
